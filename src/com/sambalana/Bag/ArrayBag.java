package com.sambalana.Bag;

/**
 * Created by Sam on 9/8/2014.
 */
public class ArrayBag<T> implements BagInterface<T> {

    private final T[] bag;
    private static final int DEFAULT_CAPACITY = 25;
    private int capacity;
    private int numberOfEntries;

    /*
    Algorithm ArrayBag()
        Input maximum amount of entries
                                        Operations
        ArrayBag(DEFAULT_CAPACITY)      1

    Total Operations: 1
              Big Oh: O(1)
     */
    public ArrayBag() {
        this(DEFAULT_CAPACITY);
    }

    /*
    Algorithm ArrayBag(capacity)
        Input maximum amount of entries
                                        Operations
        this.capacity <- capacity       1
        numberOfEntries <- 0            1
        tempBag = new Entry[capacity]   2
        Bag = tempBag                   1

    Total Operations: 5
              Big Oh: O(1)
     */
    public ArrayBag(int capacity) {
        // set default variables
        this.capacity = capacity;
        numberOfEntries = 0;

        // the cast is safe because the new array contains null entries
        @SuppressWarnings("unchecked")
        T[] tempBag = (T[]) new Object[capacity]; // unchecked cast
        bag = tempBag;
    }

    /*
    Algorithm add(newEntry)
        Input element to add to the Bag
                                            Operations
        if isFull() or newEntry = null      1 or 2
            return false                    1
        Bag[numberOfEntries] <- newEntry    1
        numberOfEntries++                   2
        return true                         1

    Total Operations: 6 or 7
              Big Oh: O(1)
     */
    public boolean add(T newEntry) {
        // check for dumb mistakes
        if (isFull() || newEntry == null)
            return false;

        // plug it in, plug it in
        bag[numberOfEntries] = newEntry;
        numberOfEntries++;

        // call your mother function
        return true;
    }

    /*
    Algorithm toArray()
        Output array of entries
                                                            Operations
        result = new entry[numberOfEntries]                 1
        for index <- 0; index < numberOfEntries; index++    n + 4
            result[index] = Bag[index]                      3n
        return result                                       1

    Total Operations: 4n + 6
              Big Oh: O(n)
     */
    public T[] toArray() {
        // don't slow me down, compiler!
        @SuppressWarnings("unchecked")
        T[] result = (T[]) new Object[numberOfEntries]; // unchecked cast

        // fill the result array
        for (int index = 0; index < numberOfEntries; index++) {
            result[index] = bag[index];
        }

        return result;
    }

    /*
    Algorithm isFull()
        Output true if full, false if not
                                            Operations
        return numberOfEntries = capacity   2

    Total Operations: 2
              Big Oh: O(1)
     */
    public boolean isFull() {
        return numberOfEntries == capacity;
    }

    /*
    Algorithm isEmpty()
        Output true if empty, false if not
                                        Operations
        return numberOfEntries = 0      2

    Total Operations: 2
              Big Oh: O(1)
     */
    public boolean isEmpty() {
        return numberOfEntries == 0;
    }

    /*
    Algorithm getCurrentSize()
        Output integer of the size of the Bag
                                    Operations
        return numberOfEntries      1

    Total Operations: 1
              Big Oh: O(1)
     */
    public int getCurrentSize() {
        return numberOfEntries;
    }

    /*
    Algorithm clear()
        numberOfEntries = 0         Operations: 1

    Total Operations: 1
              Big Oh: O(1)
     */
    public void clear() {
        numberOfEntries = 0;
    }

    /*
    Algorithm contains(anEntry)
        Input element to search for
        Output true if found, false if not
                                                            Operations
        for index <- 0; index < numberOfEntries; index++    n + 4
            if anEntry = Bag[index] then                    2n
                return true                                 1
        return false                                        1

    Total Operations: 3n + 5
              Big Oh: O(n)
     */
    public boolean contains(T anEntry) {
        // ask judge for warrant to search your Bag
        for (int index = 0; index < numberOfEntries; index++) {
            if (anEntry.equals(bag[index])) {
                return true;
            }
        }
        // if nothing found
        return false;
    }

    /*
    Algorithm findIndex(anEntry)
        Input element to search for
        Output integer of its index in the Bag
                                                            Operations
        for index <- 0; index < numberOfEntries; index++    n + 4
            if anEntry = Bag[index] then                    2n
                return index                                1
        return -1                                           1

    Total Operations: 3n + 5
              Big Oh: O(n)
     */
    public int findIndex(T newEntry) {
        // ask judge for warrant to search your Bag
        for (int index = 0; index < numberOfEntries; index++) {
            if (newEntry.equals(bag[index])) {
                return index;
            }
        }
        // if nothing found
        return -1;
    }

    /*
    Algorithm getFrequencyOf(anEntry)
        Input element to search for
        Output integer count of anEntry
                                                            Operations
        count <- 0                                          1
        for index <- 0; index < numberOfEntries; index++    n + 4
            if anEntry = Bag[index] then                    2n
                count++                                     n
        return count                                        1

    Total Operations: 4n + 6
              Big Oh: O(n)
     */
    public int getFrequencyOf(T newEntry) {
        // counting your crap
        int count = 0;
        for (int index = 0; index < numberOfEntries; index++) {
            if (newEntry.equals(bag[index])) {
                count++;
            }
        }
        return count;
    }

    /**
     * Recursive function to shift the whole Bag to the left,
     * acting as an 'override'
     */
    /*
    Algorithm shiftDown(fromIndex)
        Input integer index from which to start
                                                    Operations
        aheadIndex = fromIndex + 1                  2
        if aheadIndex < numberOfEntries then        1
            Bag[fromIndex] = Bag[aheadIndex]        3
            shiftDown(aheadIndex)                   1

    Total Operations: 7
              Big Oh: O(1)
     */
    private void shiftDown(int fromIndex) {
        int aheadIndex = fromIndex + 1;

        // check for end of Bag
        if (aheadIndex < numberOfEntries) {
            bag[fromIndex] = bag[aheadIndex];
            shiftDown(aheadIndex);  // repeat
        }
    }

    /**
     * Removes an entry at the specified index from this Bag,
     * if possible.
     *
     * @param index the index of the entry desired to be removed
     * @return either the removed entry, if the removal was
     * successful, or null
     */
    /*
    Algorithm remove(index)
        Input integer index from which to remove from Bag
        Output entry that was removed
                                                Operations
        result <- null                          1
        if index < numberOfEntries then         1
            result <- Bag[index]                3
            shiftDown(aheadIndex)               1
            numberOfEntries--                   2
        return result                           1

    Total Operations: 3 or 9
              Big Oh: O(1)
     */
    public T remove(int index) {
        T result = null;
        if (index < numberOfEntries) {
            result = bag[index];
            shiftDown(index);
            numberOfEntries--;
        }
        return result;
    }

    /*
    Algorithm remove()
        Output entry removed from back of Bag
                                                Operations
        return remove(numberOfEntries - 1)      3

    Total Operations: 3
              Big Oh: O(1)
     */
    public T remove() {
        // remove last entry
        return remove(numberOfEntries - 1);
    }

    /*
    Algorithm remove(anEntry)
        Input entry to be searched
        Output true if removed, false if not
                                        Operations
        index = findIndex(anEntry)      1
        if index != 1 then              1
            remove(index)               1
            return true                 1
        return false                    1

    Total Operations: 3 or 4
              Big Oh: O(1)
     */
    public boolean remove(T anEntry) {
        // find yo ass
        int index = findIndex(anEntry);
        // ya done for!
        if (index != -1) {
            remove(index);
            return true;
        }
        // ... didn't find the asshole
        return false;
    }
}