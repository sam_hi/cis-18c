package com.sambalana.Bag;

/**
 * A test of the methods add, toArray, and isFull, as defined
 * in the first draft of the class ArrayBag.
 */
public class ArrayBagDemo1 {
    public static void main(String[] args) {
        // a Bag that is not full
        BagInterface<String> aBag = new ArrayBag<String>();

        // tests on an empty Bag
        testIsFull(aBag, false);

        // adding strings
        String[] contentsOfBag1 = {"A", "A", "B", "A", "C", "A"};
        testAdd(aBag, contentsOfBag1);
        testIsFull(aBag, false);

        // a Bag that will be full
        aBag = new ArrayBag<String>(7);
        System.out.println("\nA new empty Bag:");

        // tests on an empty Bag
        testIsFull(aBag, false);

        // adding strings
        aBag.remove("A");
        String[] contentsOfBag2 = {"A", "B", "A", "C", "B", "C", "D"};
        aBag.remove("A");
        testAdd(aBag, contentsOfBag2);
        testIsFull(aBag, true);
    } // end main


    // Tests the method add.
    private static void testAdd(BagInterface<String> aBag,
                                String[] content) {
        System.out.print("Adding to the Bag: ");
        for (int index = 0; index < content.length; index++) {
            aBag.add(content[index]);
            System.out.print(content[index] + " ");
        } // end for
        System.out.println();
        displayBag(aBag);
    } // end testAdd


    // Tests the method isFull.
    // correctResult indicates what isFull should return.
    private static void testIsFull(BagInterface<String> aBag,
                                   boolean correctResult) {
        System.out.print("\nTesting the method isFull with ");
        if (correctResult)
            System.out.println("a full Bag:");
        else
            System.out.println("a Bag that is not full:");
        System.out.print("isFull finds the Bag ");
        if (correctResult && aBag.isFull())
            System.out.println("full: OK.");
        else if (correctResult)
            System.out.println("not full, but it is full: ERROR.");
        else if (!correctResult && aBag.isFull())
            System.out.println("full, but it is not full: ERROR.");
        else
            System.out.println("not full: OK.");
    } // end testIsFull


    // Tests the method toArray while displaying the Bag.
    private static void displayBag(BagInterface<String> aBag) {
        System.out.println("The Bag contains the following string(s):");
        Object[] bagArray = aBag.toArray();
        for (int index = 0; index < bagArray.length; index++) {
            System.out.print(bagArray[index] + " ");
        } // end for
        System.out.println();
    } // end displayBag
} // end ArrayBagDemo1