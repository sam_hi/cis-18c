package com.sambalana.Bag;

/**
 * An interface that describes the operations of a Bag of objects.
 */
public interface BagInterface<T> {
    /**
     * Gets the current number of entries in this Bag.
     *
     * @return the integer number of entries currently in the Bag
     */
    public int getCurrentSize();

    /**
     * Sees whether this Bag is full.
     *
     * @return true if the Bag is full, or false if not
     */
    public boolean isFull();

    /**
     * Sees whether this Bag is empty.
     *
     * @return true if the Bag is empty, or false if not
     */
    public boolean isEmpty();

    /**
     * Adds a new entry to this Bag.
     *
     * @param newEntry the object to be added as a new entry
     * @return true if the addition is successful, or false if not
     */
    public boolean add(T newEntry);

    /**
     * Removes one unspecified entry from this Bag, if possible.
     *
     * @return either the removed entry, if the removal
     * was successful, or null
     */
    public T remove();

    /**
     * Removes one occurrence of a given entry from this Bag,
     * if possible.
     *
     * @param anEntry the entry to be removed
     * @return true if the removal was successful, or false if not
     */
    public boolean remove(T anEntry);

    /**
     * Removes all entries from this Bag.
     */
    public void clear();

    /**
     * Counts the number of times a given entry appears in this Bag.
     *
     * @param anEntry the entry to be counted
     * @return the number of times anEntry appears in the Bag
     */
    public int getFrequencyOf(T anEntry);

    /**
     * Tests whether this Bag contains a given entry.
     *
     * @param anEntry the entry to locate
     * @return true if the Bag contains anEntry, or false otherwise
     */
    public boolean contains(T anEntry);

    /**
     * Creates an array of all entries that are in this Bag.
     *
     * @return a newly allocated array of all the entries in the Bag
     */
    public T[] toArray();

} // end BagInterface