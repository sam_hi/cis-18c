package com.sambalana.Bag;

/**
 * Created by Sam on 9/8/2014.
 */
public class LinkedBag<T> implements BagInterface<T> {

    public class Node<T> {
        private T data;     // entry in Bag
        private Node<T> next;  // link to next node

        public Node(T dataPortion) {
            this(dataPortion, null);
        }

        public Node(T dataPortion, Node<T> nextNode) {
            this.data = dataPortion;
            this.next = nextNode;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node<T> getNext() {
            return next;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }
    }

    private Node<T> firstNode; // reference to first node
    private int numberOfEntries;

    /*
    Algorithm LinkedBag()
                                Operations
        firstNode = null        1
        numberOfEntries = 0     1

    Total Operations: 2
              Big Oh: O(1)
     */
    public LinkedBag() {
        firstNode = null;
        numberOfEntries = 0;
    }

    /*
    Algorithm getCurrentSize()
        Output integer of the size of the Bag
                                    Operations
        return numberOfEntries      1

    Total Operations: 1
              Big Oh: O(1)
     */
    public int getCurrentSize() {
        return numberOfEntries;
    }

    /*
    Algorithm isFull()
        Output true if full, false if not
                        Operations
        return false    1

    Total Operations: 1
              Big Oh: O(1)
     */
    public boolean isFull() {
        return false;
    }

    /*
    Algorithm isEmpty()
        Output true if empty, false if not
                                                            Operations
        return numberOfEntries = 0 && firstNode != null     4

    Total Operations: 4
              Big Oh: O(1)
     */
    public boolean isEmpty() {
        return numberOfEntries == 0 && firstNode != null;
    }

    /*
    Algorithm add(newEntry)
        Input element to add to the Bag
                                            Operations
        newNode = new Node(newEntry)        2
        newNode.getNext() = firstNode       2
        firstNode = newNode                 1
        numberOfEntries++                   2
        return true                         1

    Total Operations: 8
              Big Oh: O(1)
     */
    public boolean add(T newEntry) {
        // add to beginning of chain:
        Node<T> newNode = new Node<T>(newEntry);
        newNode.setNext(firstNode);     // make new node reference rest of chain
        firstNode = newNode;                // new node is at beginning of chain
        numberOfEntries++;
        return true;
    }

    /*
    Algorithm remove()
        Output entry that was removed
                                                Operations
        result <- null                          1
        if not isEmpty() then                   2
            result <- firstNode.getData()       2
            fistNode = firstNode.getNext()      2
            numberOfEntries--                   2
        return result                           1

    Total Operations: 10
              Big Oh: O(1)
     */
    public T remove() {
        T result = null;
        if (!isEmpty()) {
            result = firstNode.getData();
            firstNode = firstNode.getNext();
            numberOfEntries--;
        }
        return result;
    }

    private class NodeSearchResults {
        private Node<T> node;
        private Node<T> previousNode;

        private NodeSearchResults(Node<T> previousNode, Node<T> node) {
            this.node = node;
            this.previousNode = previousNode;
        }

        public Node<T> get() {
            return node;
        }

        public Node<T> getPrevious() {
            return previousNode;
        }
    }

    /*
    Algorithm findEntry(anEntry)
        Input entry to be searched
        Output node search results
                                                                     Operations
        currentNode <- firstNode                                        1
        previousNode <- null                                            1

        while node != null                                              n + 1
            if anEntry = node.getData() then                            2n
                return new NodeSearchResults(previousNode, currentNode) 2
            else
                previousNode = currentNode                              n
                currentNode = currentNode.getNext()                     2n
        return null

    Total Operations: 6n + 5
              Big Oh: O(n)
     */
    private NodeSearchResults findEntry(T anEntry) {
        Node<T> currentNode = firstNode;
        Node<T> previousNode = null;

        while (currentNode != null) {
            if (anEntry.equals(currentNode.getData()))
                return new NodeSearchResults(previousNode, currentNode);
            else {
                previousNode = currentNode;
                currentNode = currentNode.getNext();
            }
        }
        // nothing found
        return null;
    }

    /*
    Algorithm remove(anEntry)
        Input entry to be searched
        Output true if removed, false if not
                                                                                    Operations
        searchResults <- findEntry(anEntry)                                         2
        if searchResults != null then                                               1
            if searchResults.getPrevious() != null then                             2
                searchResults.getPrevious().setNext(searchResults.get().getNext())  4
            else
                firstNode <- firstNode.getNext()                                    2
            numberOfEntries--                                                       2
            return true                                                             1
        return false                                                                1


    Total Operations: 15
              Big Oh: O(1)
     */
    public boolean remove(T anEntry) {
        NodeSearchResults searchResults = findEntry(anEntry);

        if (searchResults != null) {
            // fill in the gap between previous and next node, if not in front of chain
            if (searchResults.getPrevious() != null)
                searchResults.getPrevious().setNext(searchResults.get().getNext());
            else
                firstNode = firstNode.getNext(); // removing front of chain

            numberOfEntries--;
            return true;
        }
        return false;
    }

    /*
    Algorithm clear()       Operations
        firstNode = null    1

    Total Operations: 1
              Big Oh: O(1)
     */
    public void clear() {
        // throw it in the garbage
        firstNode = null;
    }

    /*
    Algorithm getFrequencyOf(anEntry)
        Input the entity to be searched
        Output integer of the entities counted
                                                                    Operations
        frequency = 0                                               1
        counter = 0                                                 1
        currentNode = firstNode                                     1

        while counter < numberOfEntries and currentNode != null     2n
            if anEntry == currentNode.getData() then                2n
                frequency++                                         2n
            counter++                                               2n
            currentNode = currentNode.getNext()                     2n

        return frequency                                            1

    Total Operations: 10n + 5
              Big Oh: O(n)
     */
    public int getFrequencyOf(T anEntry) {
        int frequency = 0;

        int counter = 0;
        Node<T> currentNode = firstNode;

        while (counter < numberOfEntries && currentNode != null) {
            if (anEntry.equals(currentNode.getData()))
                frequency++;
            counter++;
            currentNode = currentNode.getNext();
        }

        return frequency;
    }

    /*
    Algorithm contains(anEntry)
        Input entry to be searched for
        Output true if found, false if not
                                                        Operations
        currentNode = firstNode                         1

        while currentNode != null                       n
            if anEntry == currentNode.getData() then    2n
                return true                             1
            else
                currentNode = currentNode.getNext()     2n

        return false                                    1

    Total Operations: 5n + 3
              Big Oh: O(n)
     */
    public boolean contains(T anEntry) {
        Node<T> currentNode = firstNode;

        while (currentNode != null) {
            if (anEntry.equals(currentNode.getData()))
                return true;    // found it
            else
                currentNode = currentNode.getNext();
        }
        // nothing found
        return false;
    }

    /*
    Algorithm toArray()
        Output a fixed array of entries in the Bag
                                                                Operations
        result = new Entry[numberOfEntries]                     2
        index = 0                                               1
        currentNode = firstNode                                 1

        while index < numberOfEntities and currentNode != null  2n
            result[index] = currentNode.getData()               3n
            index++                                             2n
            currentNode = currentNode.getNext()                 2n

        return result                                           1

    Total Operations: 9n + 5
              Big Oh: O(n)
     */
    public T[] toArray() {
        // the cast is safe because the new array contains null entries
        @SuppressWarnings("unchecked")
        T[] result = (T[]) new Object[numberOfEntries]; // unchecked cast

        int index = 0;
        Node<T> currentNode = firstNode;

        while (index < numberOfEntries && currentNode != null) {
            result[index] = currentNode.getData();
            index++;
            currentNode = currentNode.getNext();
        }

        return result;
    }

}
