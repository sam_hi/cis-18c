package com.sambalana.Bag;

/**
 * A test of the methods add, toArray, isFull, isEmpty, and
 * getCurrentSize, as defined in the first draft of the class LinkedBag.
 */

public class LinkedBagDemo1 {
    public static void main(String[] args) {
        System.out.println("Creating an empty Bag.");
        BagInterface<String> aBag = new LinkedBag<String>();
        //testIsEmpty(aBag, true);
        displayBag(aBag);
        String[] contentsOfBag = {"A", "D", "B", "A", "C", "A", "D"};
        testAdd(aBag, contentsOfBag);
        //testIsEmpty(aBag, false);
        testIsFull(aBag, false);
    } // end main


    // Tests the method add.
    private static void testAdd(BagInterface<String> aBag,
                                String[] content) {
        System.out.print("Adding to the Bag: ");
        for (int index = 0; index < content.length; index++) {
            aBag.add(content[index]);
            System.out.print(content[index] + " ");
        } // end for
        System.out.println();
        displayBag(aBag);
    } // end testAdd


    // Tests the method isFull.
    // correctResult indicates what isFull should return.
    private static void testIsFull(BagInterface<String> aBag,
                                   boolean correctResult) {
        System.out.print("\nTesting the method isFull with ");
        if (correctResult)
            System.out.println("a full Bag:");
        else
            System.out.println("a Bag that is not full:");
        System.out.print("isFull finds the Bag ");
        if (correctResult && aBag.isFull())
            System.out.println("full: OK.");
        else if (correctResult)
            System.out.println("not full, but it is full: ERROR.");
        else if (!correctResult && aBag.isFull())
            System.out.println("full, but it is not full: ERROR.");
        else
            System.out.println("not full: OK.");
    } // end testIsFull are here.


    // Tests the method toArray while displaying the Bag.
    private static void displayBag(BagInterface<String> aBag) {
        System.out.println("The Bag contains the following string(s):");
        Object[] bagArray = aBag.toArray();
        for (int index = 0; index < bagArray.length; index++) {
            System.out.print(bagArray[index] + " ");
        } // end for
        System.out.println();
    } // end displayBag


    //  The static method testIsEmpty is analogous to testIsFull and is here.

} // end LinkedBagDemo1