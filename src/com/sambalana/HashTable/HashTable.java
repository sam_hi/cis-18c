package com.sambalana.HashTable;

import java.io.*;
import java.util.LinkedList;


public class HashTable {

    LinkedList<InputItem>[] table;
    int size;
    int seed;
    int collisions;

    HashTable(int size, int seed) {
        this.size = size;
        this.seed = seed;
        table = new LinkedList[size];
        for (int i = 0; i < size; i++)
            table[i] = new LinkedList<InputItem>();
    }

    public void processFile(String filename)
    {
        try {
            // Insert every line
            BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
            String line;
            while ((line = br.readLine()) != null)
                insert(line);
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getHashIndex(InputItem key)
    {
        int index;

        index = key.hashCode() % seed % size;

        if (index < 0)
            index += size;

        if (table[index].peek() != null)
            collisions++;

        return index;
    }

    public void insert(String key)
    {
        InputItem item = new InputItem(key);
        int index = getHashIndex(item);
        table[index].push(item);
    }

    public void printStats()
    {
        double average = 0;
        int chains = 0;
        int longest = 0;

        for (int i = 0; i < size; i++)
        {
            if (table[i].size() > 1)
                chains++;

            average += table[i].size();

            if (table[i].size() > longest)
                longest = table[i].size();
        }
        average /= size;

        System.out.println("\t Size of table: " + size);
        System.out.println("\t Total # of collisions: " + collisions);
        System.out.println("\t Non-single chains: " + chains);
        System.out.println("\t Length of the longest list: " + longest); // TODO: Finish
        System.out.println("\t Average chain length: " + average);
    }

    public int getLongestChain()
    {
        int longest = 0;

        for (int i = 0; i < size; i++)
            if (table[i].size() > longest)
                longest = table[i].size();

        return longest;
    }

    public void print()
    {
        for (int i = 0; i < size; i++) {
            System.out.print(i + ":\t");
            if (table[i].size() == 0)
                System.out.print("[]");
            else
                for (int j = 0; j < table[i].size(); j++)
                    System.out.print("[" + table[i].get(j) + "]");
            System.out.println();
        }
    }

    public static void main(String[] args)
    {

        // See PrimeBruteForce.java
    }

}
