package com.sambalana.HashTable;


import java.io.UnsupportedEncodingException;

public class InputItem {
    private String value;
    private int hash;

    InputItem(String value) {
        this.value = value;
        hash = hashCode();
    }

    @Override
    public int hashCode() {
        // One-at-a-Time Hash
        try {
            if (hash == 0) {
                for (byte bt : value.getBytes("utf-8")) {
                    hash += (bt & 0xFF);
                    hash += (hash << 10);
                    hash ^= (hash >>> 6);
                }
                hash += (hash << 3);
                hash ^= (hash >>> 11);
                hash += (hash << 15);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof InputItem))
            return false;
        InputItem other = (InputItem) o;
        return value.equals(other.value);
    }

    @Override
    public String toString() {
        return value;
    }
}
