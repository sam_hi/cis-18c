package com.sambalana.HashTable;

import java.util.BitSet;


public class PrimeBruteForce {

    public static BitSet primes;

    public static void generatePrimes(int maxSize)
    {
        if (primes == null) {
            primes = new BitSet(maxSize);
            primes.set(2, maxSize);

            for(int i = 2; i*i <= maxSize && i*i > 0; i++)
                if (primes.get(i))
                    for (int j = 2; i*j <= maxSize && i*j > 0; j++)
                        primes.clear(i*j);
        }
    }

    public static void main(String[] args)
    {
        HashTable hash;
        int size = 10;
        int primeLimit = Integer.MAX_VALUE / (Integer.MAX_VALUE / 1000);
        String inputFilename = "test1.txt";

        int shortestChain = size;
        int bestPrime = 0;
        generatePrimes(primeLimit);

        for(int prime = 2; prime < primeLimit; prime++) {
            if (primes.get(prime)) {
                hash = new HashTable(size, prime);
                hash.processFile(inputFilename);

                int longestChain = hash.getLongestChain();
                if (longestChain < shortestChain) {
                    shortestChain = longestChain;
                    bestPrime = prime;
                }
            }
        }

        System.out.println("\nThe best prime to use is " + bestPrime);

        hash = new HashTable(size, bestPrime);
        hash.processFile(inputFilename);
        hash.printStats();
    }
}
