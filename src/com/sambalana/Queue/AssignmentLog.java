package com.sambalana.Queue;

import java.sql.Date;

public class AssignmentLog {
    private MyPriorityQueue<Assignment> log;

    public AssignmentLog() {
        log = new MyPriorityQueue<Assignment>();
    }


    public void addProject(Assignment newAssignment) {
        log.add(newAssignment);
    }


    public void addProject(String courseCode, String task, Date dueDate) {
        Assignment newAssignment = new Assignment(courseCode, task, dueDate);
        addProject(newAssignment);
    }


    public Assignment getNextProject() {
        return log.peek();
    }


    public Assignment removeNextProject() {
        return log.remove();
    }
}