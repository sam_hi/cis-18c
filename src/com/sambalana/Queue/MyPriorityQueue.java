package com.sambalana.Queue;

import java.util.Queue;

public class MyPriorityQueue<T extends Comparable> implements PriorityQueueInterface<T> {
    Queue<T> queue;

    public void add(T newEntry) {
        queue.add(newEntry);
    }

    public T remove() {
        return queue.remove();
    }

    public T peek() {
        return queue.peek();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public int getSize() {
        return queue.size();
    }

    public void clear() {
        queue.clear();
    }
}
