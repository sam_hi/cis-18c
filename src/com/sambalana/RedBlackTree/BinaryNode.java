package com.sambalana.RedBlackTree;

// Basic node stored in unbalanced binary search trees
// Note that this class is not accessible outside
// of this package.


class BinaryNode<AnyType extends Comparable> {
    // Data; accessible by other package routines
    AnyType element;  // The data in the node
    Color color;      // The color of the node
    BinaryNode<AnyType> parent;   // Parent
    BinaryNode<AnyType> left;     // Left child
    BinaryNode<AnyType> right;    // Right child
    // Constructor
    BinaryNode(AnyType theElement) {
        element = theElement;
        left = right = null;
        color = Color.RED;
    }

    BinaryNode(BinaryNode<AnyType> n) {
        element = n.element;
        left = n.left;
        right = n.right;
        color = n.color;
    }

    static BinaryNode grandparent(BinaryNode n) {
        if (n != null) {
            if (n.parent != null) {
                return n.parent.parent;
            }
        }
        return null;
    }

    static BinaryNode uncle(BinaryNode n) {
        BinaryNode g = grandparent(n);
        if (g == null)
            return null;
        if (n.parent == g.left)
            return g.right;
        else
            return g.left;
    }

    static BinaryNode sibling(BinaryNode n) {
        if (n.parent == null)
            return null;

        if (n == n.parent.left)
            return n.parent.right;
        else
            return n.parent.left;
    }

    @Override
    public String toString() {
        return element.toString();
    }

    // Color of Node
    public static enum Color {
        BLACK, RED;
    }

}