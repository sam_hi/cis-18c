package com.sambalana.Stack;

import java.util.Random;

public class SortDemo {

    private static VectorStack<Integer> createRandomStack(int min, int max, int size) {
        Random rand = new Random();
        VectorStack<Integer> stack = new VectorStack<Integer>();

        for (int i = 0; i < size; i++) {
            stack.push(rand.nextInt(max - min + 1) + min);
        }

        return stack;
    }

    private static <T> void printVectorStack(VectorStack<T> stack) {
        System.out.print("Stack Contents: ");
        while (!stack.isEmpty()) {
            System.out.print(stack.pop());
            if (!stack.isEmpty())
                System.out.print(", ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        long startTime, endTime;

        VectorStack<Integer> stack1 = createRandomStack(0, 100, 1000);
        startTime = System.nanoTime();
        VectorStack.bubbleSort(stack1);
        endTime = System.nanoTime();
        System.out.println("Bubble Sort: " + (endTime - startTime)/1e6 + " ms");
        //printVectorStack(stack1);

        VectorStack<Integer> stack2 = createRandomStack(0, 100, 1000);
        startTime = System.nanoTime();
        VectorStack.bubbleSortRecursive(stack2, 0);
        endTime = System.nanoTime();
        System.out.println("Recursive Bubble Sort: " + (endTime - startTime)/1e6 + " ms");
        //printVectorStack(stack2);

        VectorStack<Integer> stack3 = createRandomStack(0, 100, 1000);
        startTime = System.nanoTime();
        VectorStack.selectionSort(stack3);
        endTime = System.nanoTime();
        System.out.println("Selection Sort: " + (endTime - startTime)/1e6 + " ms");
        //printVectorStack(stack3);

        VectorStack<Integer> stack4 = createRandomStack(0, 100, 1000);
        startTime = System.nanoTime();
        VectorStack.selectionSortRecursive(stack4, 0);
        endTime = System.nanoTime();
        System.out.println("Recursive Selection Sort: " + (endTime - startTime)/1e6 + " ms");
        //printVectorStack(stack4);
    }

}